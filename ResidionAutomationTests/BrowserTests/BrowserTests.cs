﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResidionAutomationTests.BrowserTests.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace ResidionAutomationTests.BrowserTests
{
    [TestFixture]
    class BrowserTests
    {
        private const string webUrl = "https://smoke-residion.cabfind.com/";
        private IWebDriver _driver;
        private LoginPage _loginPage;
        private Homepage _homepage;
        private ClientPage _clientPage;

        [SetUp]
        public void Setup()
        {
            _driver = new ChromeDriver();
            _loginPage = new LoginPage(_driver);
            _homepage = new Homepage(_driver);
            _clientPage = new ClientPage(_driver);

        }

        [TearDown]
        public void Teardown()
        { _driver.Dispose(); }

        [Test]
        public void user_can_successfully_log_in()
        {
            _driver.Navigate().GoToUrl(webUrl);
            _loginPage.LogIn();
        }

        [Test]
        public void user_lands_on_homepage()
        {
            _driver.Navigate().GoToUrl(webUrl);
            _loginPage.LogIn();
            _homepage.MyTasksDisplayed();
        }

        [Test]
        public void user_can_search_and_select_a_client()
        {
            _driver.Navigate().GoToUrl(webUrl);
            _loginPage.LogIn();
            _homepage.SearchForClient();
        }

        [Test]
        public void user_can_access_the_client_container()
        {
            _driver.Navigate().GoToUrl(webUrl);
            _loginPage.LogIn();
            _homepage.SearchForClient();
        }
    }
}