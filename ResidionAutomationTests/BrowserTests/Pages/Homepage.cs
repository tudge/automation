﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using System.Net.Http;

namespace ResidionAutomationTests.BrowserTests.Pages
{
    class Homepage
    {
        private readonly IWebDriver _driver;


        public double Timeout
        {
            get { return Convert.ToDouble(10); }
        }


        public IWebElement MyDueTodayTasks => _driver.FindElement(By.CssSelector("#dui-widget-2 > div.widget-title > h3 > span:nth-child(2)"));
        public IWebElement SearchClient => _driver.FindElement(By.XPath("//*[contains(@id, 'autocomplete')]"));
        public IWebElement SelectClient => _driver.FindElement(By.XPath("//*[contains(@class, 'auto-complete-item')]"));

        public Homepage(IWebDriver driver)
        { _driver = driver; }

        public bool MyTasksDisplayed()
        {
            try
            {
                var displayed = MyDueTodayTasks.Displayed;
                return !displayed;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void SearchForClient()
        {
            //change the search string and the client-specific URL will be captured below
            SearchClient.SendKeys("qa app client");
            string ClientUrl = _driver.Url;
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(Timeout);
            SelectClient.Click();
        }
    }
}
