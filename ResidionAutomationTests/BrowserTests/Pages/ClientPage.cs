﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResidionAutomationTests.BrowserTests.Pages
{
    class ClientPage
    {
        private readonly IWebDriver _driver;

        public IWebElement AccountDetails => _driver.FindElement(By.XPath("//*[@id=\"dui - widget - 1\"]/div[1]/h3/span"));


        public ClientPage(IWebDriver driver)
        { _driver = driver; }

        public bool AccountDetailsDisplayed()
        {
            try
            {
                var displayed = AccountDetails.Displayed;
                return !displayed;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
