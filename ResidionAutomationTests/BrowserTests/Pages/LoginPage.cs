﻿using OpenQA.Selenium;
using System.Threading;

namespace ResidionAutomationTests.BrowserTests.Pages
{
    class LoginPage
    {
        private readonly IWebDriver _driver;

        public IWebElement Username => _driver.FindElement(By.Id("username"));
        public IWebElement Password => _driver.FindElement(By.Id("password"));
        public IWebElement LoginButton => _driver.FindElement(By.XPath("/html/body/div/div[2]/div/div[2]/div[2]/div[1]/form/div[3]/div[2]/button"));

        public LoginPage(IWebDriver driver)
        { _driver = driver; }

        public void LogIn()
        {
            Username.SendKeys("steve");
            Password.SendKeys("password");
            Password.SendKeys(Keys.Tab);
            LoginButton.Click();
        }
    }
}
